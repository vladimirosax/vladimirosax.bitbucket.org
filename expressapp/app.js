// подключение express
var express = require("express");
var bodyParser = require("body-parser");
// айдишник для отправки сообщений ботом
var workerId;
//подключаем бота телеграмм
const TelegramBot = require("node-telegram-bot-api"),
		request = require("request"),
		fs = require("fs"),
		token = "447779271:AAGRxFs9b68jd9AdCCLAHwkkab1H1R1BHWc",
		// лонгполлинг должен быть только один, иначе конфликт 409
		bot = new TelegramBot(token,{polling:false});
// запускаем бота телеграм
bot.on('message',function(msg){
	const id = msg.from.id,
	_msgText = msg.text,
	msgText = _msgText.toLowerCase();
	// отправка сообщения
	if(msgText === 'start'){
		bot.sendMessage(id,'поехали!')
		workerId = id;
	}
	if(msgText === 'stop'){
		workerId = null;
	}
	console.log(id);
});
var myLogger = function (req, res, next) {
	var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
	var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
	if(req.originalUrl === "/report/")
	{
		if(workerId){
			bot.sendMessage(workerId,ip + ' = ' + req.originalUrl);
		}
	}
	if(req.originalUrl === "/open/")
	{
		if(workerId){
			bot.sendMessage(workerId,ip + ' = ' + req.originalUrl);
		}
	}
	if(req.originalUrl === "/terminal/")
	{
		if(workerId){
			bot.sendMessage(workerId,ip + ' = ' + req.originalUrl);
		}
	}
	
	next();
};
// создаем объект приложения
var app = express();
// создаем парсер для данных в формате json
var jsonParser = bodyParser.json();
app.use(myLogger);
//расположение статических файлов сайта
app.use(express.static(__dirname + "/public"));
// обработка пост-сообщения браузера
app.post("/user", jsonParser, function (request, response) {
    if(!request.body) return response.sendStatus(400);
    console.log(request.body);
    response.json(`${request.body.userName} - ${request.body.userAge}`);
});
//начинаем прослушивать подключения на 80 порту
app.listen(80);