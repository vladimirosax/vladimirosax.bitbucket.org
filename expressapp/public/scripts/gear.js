;(function(exports) {
    // --- начало сокращателя 
    // словарный сокращатель строк. Передающая (Sender) сторона ставит в соответствие
    // каждому уникальному входному значению порядковый номер, и передает 
    // входное значение при первом вызове, а при повторном - присвоенный номер.
    // принимающая (Receiver) сторона принимает нечисловое значение как индекс передаваемой
    //  строки.
    // словарный сендер - отправитель одиночный 
    function Converter(mode){
        var sender;
        var receiver;
        switch(mode){
            case '1':
                sender = new SenderDic();
                receiver = new ReceiverDic();
                break;
            case '0':
                sender = new DirectSender();
                receiver = new DirectReseiver();
                break;
            default:
                sender = new DirectSender();
                receiver = new DirectReseiver();
        };
        this.press = function(brick){
            return sender.getBrick(brick.copy());
        };
        this.unpress = function(brick){
            return receiver.getBrick(brick.copy());
        };
        this.pressToBrick = function(stringLikeBrick){
            return sender.getBrick(new Brick(stringLikeBrick));
        };
        this.unpressToBrick = function(stringLikeBrick){
            return this.unpress(new Brick(stringLikeBrick));
        };

    };
    // класс корневого объекта словарно-инкрементального упаковщика
    function SenderDic(){
        var intValue = 0;
        var store = new Object();
        var nextStoreHolder;
        var lastSenderDic;
        var incrementer = new SeriesBandIncrementSender();
        this.getStore = function(){
            return store;
        }
        this.getNext = function(){
            return nextStoreHolder.own;
        };
        this.getIncrementer = function(){
            return incrementer;
        };
        this.getString = function(string){
            // console.log('вход - ' + string + ' counter - ' + intValue);
            return  get(string,store) || create(string,store);

            function create(string,store){
                // console.log(' call create - ' + string);
                nextStoreHolder = {key: string,  value: intValue, own: new SenderDic() };
                store[string] = nextStoreHolder;
                intValue += 1;
                return string.trim();
            }
            function get(string,store){
                // console.log(' call get - ' + string);
                var _string = string.trim();
                if(store[_string])
                {
                    // console.log(' найдено - ' + this[string].value + ' - ' + this[string].key);
                    nextStoreHolder = store[_string];
                    return nextStoreHolder.value.toString().trim();
                }
                return null;
            }
        };
        this.getArray = function(inputArray){
            var outputArray = new Array(inputArray.length);
            var currentSender = this;
            for(var i = 0; i < outputArray.length; i++){
                outputArray[i] = currentSender.getString(inputArray[i]);
                // currentSender = currentSender.getStore()[inputArray[i]].own;
                currentSender = currentSender.getNext();
            }
            lastSenderDic = currentSender;
            return outputArray;
        };
        this.getBrick = function(brick){
            var brick = brick.copy();
            brick.dic = this.getArray(brick.dic);
            brick.inc = lastSenderDic.getIncrementer().getSendFor(brick.inc);
            return brick;
        };
    };
    // класс корневого объекта словарно-инкрементального распаковщика
    function ReceiverDic(){
        var intKey = 0;
        var currentKey = 0;
        var store = new Object();
        var nextStoreHolder;
        var lastReseiverDic;
        var incrementer = new SeriesBandIncrementReceiver();
        this.getStore = function(){
            return store;
        };
        this.getNext = function(){
            return nextStoreHolder.own;
        };
        this.getCurrentKey = function(){
            return currentKey;
        };
        this.getIncrementer = function(){
            return incrementer;
        };
        // кодируемая строка или числовой ключ (тоже строкой)
        this.getString = function(string){
            return get(string,store) || create(string,store);

            function create(string,store){
                // console.log(' call create ' + string);
                nextStoreHolder = { key: intKey, value: string.trim(), own: new ReceiverDic() };
                store[intKey] = nextStoreHolder;
                currentKey = intKey;
                intKey += 1;
                return string.trim();
            }
            function get(string,store){
                // console.log(' call get - ' + string);
                var _string = string.trim();
                if(store[_string])
                {
                    // console.log(' get ' + store[string].key)
                    nextStoreHolder = store[_string]
                    return nextStoreHolder.value.trim();
                }
                return null;
            }
        };
        this.getArray = function(inputArray){
            var outputArray = new Array(inputArray.length);
            var currentReseiver = this;
            for(var i = 0; i < outputArray.length; i++){
                outputArray[i] = currentReseiver.getString(inputArray[i]);
                // var _store = currentReseiver.getStore();
                // currentReseiver = currentReseiver.getStore()[currentReseiver.getCurrentKey()].own;
                currentReseiver = currentReseiver.getNext();
            };
            lastReseiverDic = currentReseiver;
            return outputArray;

        };
        this.getBrick = function(brick){
            var brick = brick.copy();
            brick.dic = this.getArray(brick.dic);
            brick.inc = lastReseiverDic.getIncrementer().getReceiveFor(brick.inc);
            return brick;
        };
    };
    // прозрачный получатель
    function DirectSender(){
        this.getBrick = function(brick){
            return brick;
        };
    };
    // прозрачный отправитель 
    function DirectReseiver(){
        this.getBrick = function(brick){
            return brick;
        };
    };
    // инкрементальный сжиматель строк 
    // Пустые - не передавать(дубликация предыдущей строки на принимающем конце)
    function SingleIncrementPresser(){
        var changeArgCode = '\u000C';
        var changeArgStr ='\f';
        var changeArgInt = changeArgStr.charCodeAt(0);

        var lastString;
        var _rawString;

        this.press = function(rawString){
            _rawString = rawString.trim();
            // первый проход - старая строка не определена,все остальные - вызов функции 
            var pressedString = lastString ? diffPress(_rawString,lastString) : _rawString;
            lastString = _rawString.toString().trim();
            return pressedString;
        }

        var diffPress = function(raw,last){
            if(raw.length != last.length) {
                // смена аргумента 
                // console.log('смена аргумента');
                return changeArgStr + raw;
            }
            // они равной длины, ищем расхождение в символах
            var splittedRaw = raw.split('');
            var splittedLast = last.split('');
            var diffEnd = '';
            for(var i = 0 ; i < splittedRaw .length ; i++)
            {
                if((splittedRaw[i] != splittedLast[i])){
                    return diffEnd = raw.slice(i);
                }
            }
            // console.log('-- ' + diffEnd);
            return diffEnd;
        }
    };
    // инкрементальный разжиматель строк 
    function SingleIncrementUnpresser(){
        var changeArgStr ='\f';
        var changeArgInt = changeArgStr.charCodeAt(0);
        var lastString;
        var _rawString;
        this.unpress = function(rawString){
            _rawString = rawString;
            // console.log(_rawString);
            // первая строка в сеансе работы, возвращаем целиком (lastString == undefined)
            if(!lastString){
                return lastString = _rawString.trim();
            }
            // пришел пустой символ - просто повторяем старую строку
            if(_rawString == ''){
                return lastString;
            }
            // найден символ обновления, отрезаем его и возвращаем остаток
            if(_rawString.charCodeAt(0) == changeArgInt){
                return lastString = _rawString.substr(1).trim();
            }
            // собираем строку из огрызка и старой строки
            // console.log(lastString + '  -lastString');
            // console.log(_rawString + '  -_rawString')
            return lastString = lastString.slice(0,-_rawString.length).trim() + _rawString.trim();
        };
    };
    // возвращает массив (фиксированой длины) инкрементально сжатых строк ()
    function BandIncrementPresser(size){
        var _size = size;
        var pressBand = new Array(_size);
        // var i;
        for(var i = 0; i < size; i++){
            pressBand[i] = new SingleIncrementPresser();
        };
        // console.log(initialArray);
        this.process = function(arrayOriginalStrings){
            if(arrayOriginalStrings.length != _size) {
                // console.log('не совпадает размер массива');
                return null;
            }
            var pressedStrings = new Array(_size);
            // console.log(_size);
            // var i;
            for(var i = 0; i < _size; i++){
                // console.log('bip - ' + i + ' = ' + arrayOriginalStrings[i]);
                pressedStrings[i] = pressBand[i].press(arrayOriginalStrings[i]);
                // console.log('bip - ' + i +' = '+ pressedStrings[i]);
            };
            // console.log('bip - ' + pressedStrings);
            return pressedStrings;
        };

    };
    // возвращает (фиксированной длины) массив разжатых строк (из массива сжатых)
    function BandIncrementUnpresser(size){
        var _size = size;
        var unpressBand = new Array(_size);
        for(i = 0; i < size; i++){
            unpressBand[i] = new SingleIncrementUnpresser();
        };
        this.process = function(arrayPressedStrings){
            if(arrayPressedStrings.length != _size) {
                // console.log('не совпадает размер массива');
                return null;
            }
            var unpressedStrings = new Array(_size);
            for(i = 0; i < _size; i++){
                unpressedStrings[i] = unpressBand[i].unpress(arrayPressedStrings[i]);
            };
            return unpressedStrings;
        };

    };
    // автодополняемая совокупность массивов инкрементально сокращаемых строк для отсылки (сжатия) сообщений
    function SeriesBandIncrementSender(){
        var bands = new Object();
        this.getSendFor = function(originalStringArray){
            var _originalStringArray = originalStringArray;
            var size = _originalStringArray.length;
            var pressBand = bands[size] || createBand(size);
            return pressBand.process(_originalStringArray);

        };
        var createBand = function(size){
            // console.log('new pressband size = ' + size)
            bands[size] = new BandIncrementPresser(size);
            return bands[size] ;
        };

    };
    // автодополняемая совокупность массивов инкрементально сокращаемых строк для приема (расжатия) сообщений 
    function SeriesBandIncrementReceiver(){
        var bands = new Object();
        this.getReceiveFor = function(pressedStringArray){
            var _pressedStringArray = pressedStringArray;
            var size = _pressedStringArray.length;
            var unpressBand = bands[size] || createBand(size);
            return unpressBand.process(pressedStringArray);

        };
        var createBand = function(size){
            // console.log('new unpressband size = ' + size)
            bands[size] = new BandIncrementUnpresser(size);
            return bands[size] ;
        };

    };
    // ---- конец сокращателя 
    // ---кирпич
    function Brick(string){
        this.dic;
        this.inc;
        if(string)
        {
            var arg = string;
            var arr = arg.split('^');
            this.dic = arr[0].split('|');
            this.inc = arr[1].split('|');
        }
        else{

        }
    };
    Brick.prototype.toString = function(){
        return this.dic.join('|') + '^' + this.inc.join('|');
    };
    Brick.prototype.copy = function(){
        return new Brick(this.toString());
    };
    Brick.prototype.setBrick = function(arrayDict,arrayIncr){
        this.dic = arrayDict;
        this.inc = arrayIncr;
        return this;
    }
    // --- конец кирпича
    // начало вебсокета
    function WebSockHub(actionHost){
        var support = "MozWebSocket" in window ? 'MozWebSocket' : ("WebSocket" in window ? 'WebSocket' : null);
        if(!support)
        { 
            alert('WebSocket is not supported');
            return;
        };
        this.disposed = false;
        var ws;
        start(actionHost.wsLocation,this);

        function start(wsLocation,hub){
            if(hub.disposed){
                //console.log("запуск уничтоженного объекта");
                return;
            }
            console.log('conecting to:  '+ wsLocation);
            ws = new window[support](wsLocation);
            ws.onclose = function(evt){
                actionHost.onClose(evt);
                setTimeout(function(){
                    start(actionHost.wsLocation,hub);
                }, 5000);
            };
            ws.onerror = function(evt){
                actionHost.onError(evt);
            };
            ws.onopen = function(){
                ws.send(actionHost.handShake);
                actionHost.onOpen(this);
                // console.log(ws);
            };
            ws.onmessage = function(evt){
                if(actionHost.checkPass(evt.data)){
                    var tmp = actionHost.onLogin();
                    if(tmp){
                        ws.onmessage = actionHost.onMessage;
                        ws.send(tmp);
                        actionHost.afterLogin();
                    };
                }
                else{
                    // неудачный ответ от сервера 
                    actionHost.onFailedLogin(evt.data);
                }
            };
        };
        this.dispose = function(){
            ws.close();
            // ws.dispose();
            this.disposed = true;
            //console.log("диспозинг сокета");
        };
    } ;
    // конец вебсокета 
    // экшн-хост (ангулярозависим через таймаут)
    function ActionHostBase(business, credentials, $timeout){
        // бизнес - расшифровка приходящих строк и обратный вызов методов
        /* - должен поддерживать следующие методы :
            business.onConnectionOpen();
            business.callMethod(brick);
            business.onConnectionClose();
            business.afterLogin();
        */
        this.business = business;
        this.business.actionHost = this;
        this.credentials = credentials;
        this.wsLocation = credentials.wsLocation;
        this.handShake = credentials.handShake;
        this.lazyTime = Date();
        var converter; //прессер
        var ws; //вебсокет сам по себе
        var webSockConnector = new WebSockHub(this);

        var timerId;
        // вызов через паузу. повтор до истечения обновляет паузу
        this.lazyUI = function(pause){
            clearTimeout(timerId);
            timerId = setTimeout(this.spotUI, pause);
        };
        this.spotUI = function(func){
            // заправляем внешнюю функцию в поток ангуляра
            var tmp = func || null;
            $timeout(tmp);
        };
        //просто задержка в потоке ангуляра 
        this.delayUI = function(func){
            var tmp = func || null;
            $timeout(tmp, 1000);
        };
        // вызывается коннектором 
        this.onOpen = function(evt){
            ws = evt;
            business.onConnectionOpen();
            // console.log(evt);
            console.log('connect OK');
        };
        // вызывается коннектором 
        this.onMessage = function(evt){
            // console.log(evt.data);
            if(evt.data)
            {
                try{
                    business.callMethod(converter.unpressToBrick(evt.data));
                }
                catch(err){
                    console.log(err);
                    location.reload();
                }
                
            }
        };

        // вызывается коннектором 
        this.onClose = function(evt){
            console.log("closed");
            console.log(evt);
            business.onConnectionClose();
        };
        // вызывается коннектором 
        this.onError = function(evt){
            // console.log("ошибка");
            // console.log(evt);
            //alert("ошибка" + evt);
        };
        // проверяем ответные креденциалы . вызывается коннектором 
        this.checkPass = function(credentials){
            return true;
        };
        this.onFailedLogin = function(data){
            //alert("неудачная авторизация  " + data);
        };
        // возвращает то, что нужно отослать серверу после удачного логина серверного уровня
        // и создает конвертер, преобразующий сообщения в компактный формат и шифрующий их.
        // вызывается коннектором 
        this.onLogin = function(){
            converter = $gear.getGear(credentials.pressMode);
            // теперь бизнес может отправлять сообщения
            // business.segmentHistoryRequest(business.currentRequest);
            return converter.pressToBrick(business.greetingApp());
        };
        // вызывается после удачной отработки вызова this.onLogin
        // вызывается коннектором  
        this.afterLogin = function(){
            business.afterLogin();
            // business.segmentHistoryRequest(business.currentRequest);
        };
        // принимает строго кирпичи 
        // вызывается бизнесом 
        this.sendMessage = function(brick){
            if(webSockConnector){
                ws.send(converter.press(brick).toString());
            };
        };
        this.dispose = function(){
            console.log("диспозинг экшн  - хоста");
            webSockConnector.dispose();
        };
    };
    // конец экшн-хоста 
    // базовый бизнес (ассоциативный массив) для наследования и переопределения
    function BusinessBase(){

        this.connectionFlag = false;
        this.firstOutcomeMessage = 'GreetingApp^УРА!';
        // можно что-то делать по тику таймера 
        var _onTick = function()
        {
            
        };
        Object.defineProperty(this,"onTick",{
            get:function(){
                return _onTick;
            },
            set:function(value){
                _onTick = value;
            }
        });
        //источник времени для всех компонентов приложения
        var _cron = new Object(null);
        Object.defineProperty(this,"cron",{
            get:function(){
                return _cron;
            }
        });
        //таймер с заданной частотой срабатывания
        this.lazyTimer = new LazyTimer(1000, _cron, _onTick);

        var _actionHost;
        Object.defineProperty(this,"actionHost",{
            get:function(){
                return _actionHost;
            },
            set:function(value){
                _actionHost = value;
            }
        });
        // обязательные методы
        this.onConnectionOpen = function(){
        };
        this.onConnectionClose = function(){
        };
        this.afterLogin = function(){
        };
        this.greetingApp = function(){
            return this.firstOutcomeMessage;
        };
        // общее место вызова всех методов снаружи
        this.callMethod = function(brick){
            if(typeof(this[brick.dic[0]]) == 'function'){
                this[brick.dic[0]](brick);
            }
            else{
                console.log(brick);
            }
        };
        // немедленное обновление гуя
        this.spotUI = function(func){
            if(_actionHost){
                _actionHost.spotUI(func);
            };
        };
        this.lazyUI = function(pause){
            if(_actionHost){
                _actionHost.lazyUI(pause);
            };
        };
        // --- штамп времени
        this.TimeStamp = function(brick){
            this.lazyTimer.serverStamp = new TimeStamp(brick);
        };

    };
    // конец протобизнеса
    // начало универсального обновляемого сториджа
    function UpdatableStorage(){
        this.displayName = '';
        // ассоциативный массив 
        var _ass = Object.create(null);
        Object.defineProperty(this,"ass",{
            get:function(){
                return _ass;
            }
        });
        // индексный массив  
        var _arr = [];
        Object.defineProperty(this,"arr",{
            get:function(){
                return _arr;
            }
        });
        this.clearContent = function(){
            while(_arr.length > 0){
                _arr.pop();
            };
            _ass = {};
        };
        // для переопределения в потомках
        this.beforeAdd = function(name,obj){};
        this.afterAdd = function(name,obj){};
        this.addOrUpdateBeforeUpdate = function(name,obj){};
        this.addOrUpdateAfterUpdate = function(name,obj){};
        // добавление объекта в хранилище 
        this.add = function(name,obj){
            if(!(name in this.ass)){
                this.beforeAdd(name,obj);
                _ass[name] = obj;
                _arr.push(obj);
                this.afterAdd(name,obj);
            } 
        };
        this.update = function(name,obj){
            _ass[name].update(obj);
        };
        this.addOrUpdate = function(name,obj){
            if(!(name in this.ass)){
                this.beforeAdd(name,obj);
                _ass[name] = obj;
                _arr.push(obj);
                this.afterAdd(name,obj);
            } 
            else{
                //  здесь обновить свойства инструмента
                this.addOrUpdateBeforeUpdate(name,obj);
                this.update(name,obj);
                this.addOrUpdateAfterUpdate(name,obj);
            }
        };
        this.remove = function(name){
            _arr.splice(_arr.indexOf(_ass[name]), 1);
            delete _ass[name];
        };
    };
    // конец универсального сториджа 
    // --- Группа представляет и содержит объекты с одинаковым свойством группировки.
    // groupMember - объект по значению свойства которого происходит группировка (производящий объект), 
    // groupBy  - имя свойства в производящем объекте
    function Group(groupMember, groupBy, displayName, groupContentKeyFieldName){

        var _sortSign = -1;
        this.resort = function(sortField){
            var sortField = sortField || 'symbol';
            this.arr.sort(function(a,b){  
            return _sortSign * (a[sortField] > b[sortField] ? -1:1);
            });
            _sortSign *=-1;
        };
        Object.defineProperty(this,'sortSign',{
            set : function(value){
                _sortSign = value;
            },
            get:function(){
                return _sortSign;
            }
        });
        // это для показа в гуи
        this.displayName = displayName;
        // ключевое поле в хранимом объекте 
        this.groupContentKeyFieldName = groupContentKeyFieldName;
        // извлекаем значение группировочного поля из производящего объекта 
        this.groupingField = !groupMember ? null: (!groupBy ? null: groupMember[groupBy]);
        // массив сгруппированных объектов 
        this.arr = [];
        this.ass = Object.create(null);

    };
    Group.prototype.removeContent = function(groupContent){
        if(groupContent[this.groupContentKeyFieldName] in this.ass){
            this.arr.splice(this.arr.indexOf(groupContent), 1);
            delete this.ass[groupContent[this.groupContentKeyFieldName]];
        }

    };
    Group.prototype.addContent = function(groupContent){
        if(!(groupContent[this.groupContentKeyFieldName] in this.ass)){
            this.ass[groupContent[this.groupContentKeyFieldName]] = groupContent;
            this.arr.push(this.ass[groupContent[this.groupContentKeyFieldName]]);
        }
    };
    Group.prototype.cloneGroup = function(nameNew){
        var freshGroup = new Group(null,null,nameNew,null);
        freshGroup.groupContentKeyFieldName = this.groupContentKeyFieldName;
        freshGroup.groupingField = this.groupingField;
        return freshGroup;
    };
    // конец отображаемой группы

    // хранилище отображаемых групп 
    function GroupStorage(createGroupFunc){
        var _sortSign = -1;
        this.resort = function(sortField){
            var sortField = sortField || 'symbol';
            this.arr.sort(function(a,b){  
            return _sortSign * (a[sortField] > b[sortField] ? -1:1);
            });
            _sortSign *=-1;
        };
        Object.defineProperty(this,'sortSign',{
            set : function(value){
                _sortSign = value;
            },
            get:function(){
                return _sortSign;
            }
        });
        // функция, создающая группу из объекта контента
        this.createGroup = createGroupFunc;
        // ключ (название свойства) члена группы. значение должно быть уникальным
        // this.valueGroupContentKey = valueGroupContentKey;
        // ассоциативный массив групп
        this.ass = Object.create(null);
        // простой массив групп для биндинга и сортировки
        this.arr = [];
        // получение группируемого объекта (члена группы) и попытка добавить группу в сторидж 
        this.add = function(groupContent){
            var tmpGroup = this.createGroup(groupContent);
            // groupingField определяется в конструкторе группы;
            if(tmpGroup.groupingField in this.ass){
                // добавление объекта в существующую группу
                this.ass[tmpGroup.groupingField].addContent(groupContent);
                // в массив впушивать не надо, группа уже там
            }
            else{
                this.ass[tmpGroup.groupingField] = tmpGroup;
                this.arr.push(this.ass[tmpGroup.groupingField]);
                // добавление во вновь созданную группу 
                this.ass[tmpGroup.groupingField].addContent(groupContent);
            };
        };
        this.removeGroup = function(groupKey){
            if(this.ass[groupKey]){
                if(this.ass[groupKey].arr.length < 1){
                    this.arr.splice(this.arr.indexOf(this.ass[groupKey]), 1);
                    delete this.ass[groupKey];
                }
            }
        };
        // перенос элемента из группы в группу по значению ключа
        this.reLocation = function(oldGroupName,newGroupName,valueGroupContentKey){
            if(oldGroupName === newGroupName) return;
            // console.log(oldGroupName,newGroupName,valueGroupContentKey);
            // если исходная группа отсутствует, то состояние терминала устарело. Надо перегрузить его.
            var oldG = this.ass[oldGroupName];
            // буферный айтем со старой группой
            var buff = oldG.ass[valueGroupContentKey];
            // удалить объект из старой группы 
            oldG.removeContent(buff);
            // попытка удаления не сработает, если группа не пуста
            this.removeGroup(oldGroupName);
            // если целевая группа отсутствует, она должна быть создана
            var targetGroup = this.ass[newGroupName];
            if(!targetGroup){
                this.ass[newGroupName] = oldG.cloneGroup(newGroupName);
                this.ass[newGroupName].addContent(buff);
                this.arr.push(this.ass[newGroupName] );
            }else{
                targetGroup.addContent(buff);
            };
        };
        this.removeContent = function(groupName,valueGroupContentKey){
            var targetGroup = this.ass[groupName];
            var buff = targetGroup.ass[valueGroupContentKey];
            targetGroup.removeContent(buff);
            this.removeGroup(groupName);
        };
    };
    // 
    // ---креденциалы
    function Credentials($cookies,shortCredentials,hardcodeCookieName){
            this.role ='STATUS';
            this.login = '';//b0edc93e-527a-4b93-a70c-78fc8caf2a20';
            this.responce = 'Success';
            this.nowTimeMs = '636021124868975441';
            this.serverToken = '';
            this.password = '';//a3000610';
            this.cryptMode = 'SSLMODE';
            this.pressMode = '1';
            // не передается на сервер 
            var _endPoint = '193.124.59.238:5024';
            var _cookieName = 'alp';
            if(shortCredentials){
                this.role = shortCredentials.role;
                this.login =shortCredentials.login;
                this.password = shortCredentials.password;
            };
            if(hardcodeCookieName){

            };
            // только для чтения 
            Object.defineProperty(this, "handShake", {
                enumerable: false,
                get: function (){
                    var arr = [this.role, this.login, this.responce, this.nowTimeMs,this.serverToken,this.password,this.cryptMode,this.pressMode ];
                return arr.join(';');
                }
            });
            Object.defineProperty(this,"endPoint",{
                enumerable: false,
                get:function(){
                    return _endPoint;
                },
                set:function(value){
                    _endPoint = value;
                }
            });
            Object.defineProperty(this,"wsLocation",{
                enumerable: false,
                get:function(){
                    return 'ws://' + this.endPoint;
                }
            });
            // обновление параметров доступа через гуи
            this.updateFromGui = function(address,login,passwosd){
                this.endPoint = address;
                this.login = login;
                this.password = password;
                // alert('updateFromGui');
            };
            // обновление через куки
            this.updateFromCookies = function($cookies){
                // alert('try update from cookies');
                if($cookies) {
                    // console.log('кука есть');
                    var credentialsInCookie = $cookies.getObject(_cookieName);
                    if(credentialsInCookie){
                        // alert(credentialsInCookie.endPoint);
                        this.endPoint = credentialsInCookie.endPoint;
                        this.login = credentialsInCookie.login;
                        this.password = credentialsInCookie.password;
                    };
                }
                else{
                    //alert('NO cookies');
                }
            };
            // сохранение в куки 
            this.saveInCookie = function($cookies){
                var expireDate = new Date();
                expireDate.setDate(expireDate.getDate() + 365*5);
                if($cookies) { 
                    // alert('try saveInCookie');
                    $cookies.putObject('alp', 
                    { 
                        endPoint: this.endPoint, 
                        login: this.login, 
                        password: this.password
                    },
                    {
                        'expires' : expireDate
                    });
                };
            };
            this.updateFromCookies($cookies);
    };
    // ---конец креденциалов
    function LazyTimer(interval,chron,onTick,locale){
        var _outObj = chron || new Object(null);
        var _onTick = onTick || function(){};
        var _locale = locale || 'ru';
        var _lazyTime;
        Object.defineProperty(this,"lazyTime",{
            get:function(){
                return _lazyTime;
            },
            set:function(value){
                _lazyTime = value;
            }
        });

        var _lazyTimeStringHMS;
        Object.defineProperty(this,"lazyTimeStringHMS",{
            get:function(){
                return _lazyTimeStringHMS;
            },
            set:function(value){
                _lazyTimeStringHMS = value;
            }
        });
        var _lazyTimeStringFull;
        Object.defineProperty(this,"lazyTimeStringFull",{
            get:function(){
                return _lazyTimeStringFull;
            },
            set:function(value){
                _lazyTimeStringFull = value;
            }
        });
        Object.defineProperty(this,"locale",{
            get:function(){
                return _locale;
            },
            set:function(value){
                _locale = value;
            }
        });
        var _serverOutrun = 0;
        var _serverStamp;
        Object.defineProperty(this,"serverStamp",{
            get:function(){
                return _serverStamp;
            },
            set:function(value){
                _serverStamp = value;
                _serverOutrun = _serverStamp.serverOutrun
            }
        });
        var optionsHMS = {
            hour: 'numeric',
            minute: 'numeric',
            second: 'numeric'
        };
        var optionsFull = {
            era: 'long',
            year: 'numeric',
            month: 'long',
            day: 'numeric',
            weekday: 'long',
            timezone: 'UTC',
            hour: 'numeric',
            minute: 'numeric',
            second: 'numeric'
        };
        var setTimer = function(outrunTicks){
            var serverOutrunning = outrunTicks || 0;

            var timezone =  new Date().getTimezoneOffset();
            _lazyTime = new Date(Date.now() + 60000*timezone + serverOutrunning);
            // console.log(_lazyTime);

            _lazyTimeStringHMS = _lazyTime.toLocaleString(_locale,optionsHMS);
            _lazyTimeStringFull = _lazyTime.toLocaleString(_locale,optionsFull);

            _outObj['lazyTime'] = _lazyTime;
            _outObj['lazyTimeStringHMS'] = _lazyTimeStringHMS;
            _outObj['lazyTimeStringFull'] = _lazyTimeStringFull;
            _onTick();
        };

        setTimer(_serverOutrun);

        var timerId = setTimeout(function tick() {
            setTimer (_serverOutrun);
            timerId = setTimeout(tick, interval + getShift(interval));
        }, interval + getShift(interval));

    };
    // ---
    function getShift(interval){
        var t0 = new Date();
        // var t = Math.round(t0/interval)*interval - t0;
        // console.log(t);
        return  Math.round(t0/interval)*interval - t0;
    }
    // ---штамп времени 
    function TimeStamp(brick){
        // console.log(brick);
        var rr = brick.inc[0] + "T" + brick.inc[1] + "."+ brick.inc[2] + "Z";
        this.msUTC = Date.parse(rr);
        // console.log(new Date(this.msUTC).toUTCString());
        this.serverOutrun = new Date(this.msUTC) - new Date();
        // console.log(this.serverOutrun);
    };
    // --- выдача результатов
    exports.getGear = function(mode){
        return new Converter(mode);
    };
    exports.getBrick = function(string){
        return new Brick(string);
    };
    exports.brickToString = function(brick){
        return brick.toString;
    };
    exports.makeBrick = function(arrDict,arrInc){
        return new Brick().setBrick(arrDict,arrInc);
    };
    exports.getConnector = function(actionHost){
        return new WebSockHub(actionHost);
    };
    exports.getActionHost = function(business, credentials, $timeout){
        return new ActionHostBase(business, credentials, $timeout);
    };
    exports.uParseFloat = function(doubleAsString){
        return parseFloat(doubleAsString.replace(/[,]+/g, '.'));
    };
    exports.BusinessBase = BusinessBase;
    exports.Credentials = Credentials;
    exports.UpdatableStorage = UpdatableStorage;
    exports.GroupStorage = GroupStorage;
    exports.Group = Group;
    exports.LazyTimer = LazyTimer;
})(this.$gear = {});


// -----торгово-котировочные объекты 
;(function(exports) {
    function SnapshotTooSideQuote(brick){
        this.symbol = brick.dic[1];
        this.bidVal = brick.inc[0];
        this.askVal = brick.inc[1];
        // тут в флоат не переводим
        // this.bidVal = parseFloat(brick.inc[0].replace(/[,]+/g, '.'));
        // this.askVal = parseFloat(brick.inc[1].replace(/[,]+/g, '.'));
        this.snapTime = brick.inc[2];
    };
    SnapshotTooSideQuote.prototype.toString = function(){
        return "SnapshotTooSideQuote" + 
        '|' + 
        this.symbol +
        '^' + 
        this.bidVal +
        '|' + 
        this.askVal +
        '|' + 
        this.snapTime;
    };
    // свойства инструмента (передается по сети)
    function ToolProperties(brick){
        // var brick = brick;

        this.group = brick.dic[1];

        this.symbol = brick.inc[11];
        this.precision = brick.inc[1];
        this.CCY1 = brick.inc[5];
        this.CCY2 = brick.inc[6];

        this.description = brick.inc[0];
        this.stopLevel = parseInt(brick.inc[2]);
        
        this.swapLong = parseFloat(brick.inc[3].replace(/[,]+/g, '.'));
        this.swapShort = parseFloat(brick.inc[4].replace(/[,]+/g, '.'));
        this.contractSize = parseInt(brick.inc[7]);
        this.enableTransmit = brick.inc[8];
        this.allowTrade = brick.inc[9];
        this.isDeleted = brick.inc[10];
    };
    ToolProperties.prototype.toString = function(){
        return 'ToolPropertiesUpdate' + 
        '|' + 
        this.group +
        '^' + 
        this.description +
        '|' + 
        this.precision + 
        '|' + 
        this.stopLevel + 
        '|' + 
        this.swapLong +       
        '|' + 
        this.swapShort +
        '|' +
        this.CCY1 +
        '|' +
        this.CCY2 +
        '|' +
        this.contractSize +
        '|' +
        this.enableTransmit +
        '|' + 
        this.allowTrade + 
        '|' + 
        this.isDeleted +
        '|' +
        this.symbol;
    };
    ToolProperties.prototype.update = function(toolProperties){
        this.group = toolProperties.group;
        this.symbol = toolProperties.symbol;
        this.precision = toolProperties.precision;
        this.CCY1 = toolProperties.CCY1;
        this.CCY2 = toolProperties.CCY2;
        this.description = toolProperties.description;
        this.stopLevel = toolProperties.stopLevel;
        this.swapShort = toolProperties.swapShort;
        this.swapLong = toolProperties.swapLong;
        this.contractSize = toolProperties.contractSize;
        this.enableTransmit = toolProperties.enableTransmit;
        this.allowTrade = toolProperties.allowTrade;
        this.isDeleted = toolProperties.isDeleted;
    };
    // полуквота  (передается по сети)
    function ShortQuote(brick){
        // var brick = brick;
        this.symbol = brick.dic[1];
        this.side = brick.dic[0];
        this.price = brick.inc[0].replace(/[,]+/g, '.');
        // this.price = parseFloat(brick.inc[0].replace(/[,]+/g, '.'));
    };
    ShortQuote.prototype.toString = function(side){
        return side || 'ShortQuote' + 
        '|' + 
        this.symbol +
        '^' + 
        this.price ;
    };
    // видимая двусторонняя котировка
    function VisualQuote(toolProperties, cron){
        var _symbol;
        var _bid = '----------';
        var _ask = '----------';
        var _time = '---------';
        var _properties;
        // ---
        var _bidfloat = 0;
        var _askfloat = 0;
        // ----
        var _oldbid = 0;
        var _oldask = 0;

        var _flagbid = "N";
        var _flagask = "N";

        Object.defineProperty(this,'flagBID',{
            set : function(value){
                _flagbid = value;
            },
            get:function(){
                return _flagbid;
            }
        });
        Object.defineProperty(this,'oldBID',{
            set : function(value){
                _oldbid = value;
            },
            get:function(){
                return _oldbid;
            }
        });
        Object.defineProperty(this,'flagASK',{
            set : function(value){
            _flagask = value;
            },
            get:function(){
                return _flagask;
            }
        });
        Object.defineProperty(this,'oldASK',{
            set : function(value){
                _oldask = value;
            },
            get:function(){
                return _oldask;
            }
        });

        Object.defineProperty(this,'symbol',{
            set : function(value){
                _symbol = value;
            },
            get:function(){
                return _symbol;
            }
        });
        Object.defineProperty(this,'BID',{
            set : function(value){
                
                // _bid = value;
                _time = cron['lazyTimeStringHMS'];
                _bid = _bidfloat = parseFloat(value.replace(/[,]+/g, '.'));

                var buff = _oldbid == 0 ? 0 :(_bidfloat - _oldbid);
                _flagbid = buff == 0 ? "N" : (buff > 0 ? "U":"D");

                _oldbid = _bid;

                this.pushbid(_bidfloat);
                this.pushlabel(_time);
            },
            get : function(){
                return _bid;
            }
        });        
        Object.defineProperty(this,'ASK',{
            set : function(value){
                // _ask = value;
                _time = cron['lazyTimeStringHMS'];
                _ask = _askfloat = parseFloat(value.replace(/[,]+/g, '.'));

                var buff = _oldask == 0 ? 0 :(_askfloat - _oldask);
                _flagask = buff == 0 ? "N" : (buff > 0 ? "U":"D");

                _oldask = _askfloat;

                // this.pushask(_ask);
                // this.pushlabel(_time);
            },
            get:function(){
                return _ask;
            }
        });
        
        Object.defineProperty(this,'bidFloat',{
            get:function(){
                return _bidfloat;
            }
        });
        
        Object.defineProperty(this,'askFloat',{
            get:function(){
                return _askfloat;
            }
        });
        Object.defineProperty(this,'properties',{
            set : function(value){
                _properties = value;
                _symbol = _properties.symbol;
            },
            get:function(){
                return _properties;
            }
        });
        Object.defineProperty(this,'time',{
            get:function(){
                return _time;
            },
            set: function(value){
                _time = value;
            }
        });
        this.properties = toolProperties;
        // --- копим историю тиков по инструменту 
        var tickhistorydept = 60;
        var bidarr = new Array(tickhistorydept);
        var askarr =[];

        this.labels = new Array(tickhistorydept);
        this.data = [
            bidarr 
            // ,
            // askarr
          ];
        this.pushlabel = function(arg){
            this.labels.shift();
            this.labels.push(arg);
        };
        this.pushbid = function(arg){
            bidarr.shift();
            bidarr.push(arg.toString());
        };
        this.pushask = function(arg){
            askarr.shift();
            askarr.push(arg.toString());
        };
        // ---
        this.resetHistory = function(snapshotTooSideQuote)
        {
            bidarr = new Array(tickhistorydept);
            this.labels = new Array(tickhistorydept);
            this.data = [bidarr];
        };
    };
    VisualQuote.prototype.updateShortQuote = function(shortQuote){
        this[shortQuote.side] = shortQuote.price;
    };
    VisualQuote.prototype.update = function(visualQuote){
        this.properties = visualQuote.properties;
    };
    VisualQuote.prototype.updateSnapshot = function(snapshotTooSideQuote){

        this.BID = snapshotTooSideQuote.bidVal;
        this.ASK = snapshotTooSideQuote.askVal;
        this.time = snapshotTooSideQuote.snapTime;
        this.oldASK = this.ASK;
        this.oldBID = this.BID;
    };
    // --- сторидж видимых котировок с группировкой по group 
    function VisualQuoteStorage(cron){
        $gear.UpdatableStorage.apply(this);

        var sortSign = -1;
        this.resort = function(sortField){
            console.log(this.groups); 
            var sortField = sortField || 'symbol';
            this.arr.sort(function(a,b){  
            return sortSign * (a[sortField] > b[sortField] ? -1:1);
            });
        sortSign *=-1;
        };

        var groupCreator = function(visualQuote){
            return new $gear.Group(visualQuote.properties,'group',visualQuote.properties['group'],'symbol');

        };
        // группы по признаку
        this.groups = new $gear.GroupStorage(groupCreator);

        this.afterAdd = function(name, obj){
            this.groups.add(this.ass[name]);
            
        };
        this.addOrUpdateBeforeUpdate = function(name,obj){
            this.groups.reLocation(this.ass[obj.symbol].properties.group, obj.properties.group, obj.symbol);
        };
        this.addOrUpdateToolProperties = function(toolProperties){
            this.addOrUpdate(toolProperties.symbol,new VisualQuote(toolProperties,cron));
        };
        this.removeQuote = function(toolProperties){
            this.groups.removeContent(toolProperties.group, toolProperties.symbol);
            this.remove(toolProperties.symbol)
        };
        // обновление снапшотом 
        this.updateSnapshot = function(snapshotTooSideQuote){
            if(this.ass[snapshotTooSideQuote.symbol]){
                this.ass[snapshotTooSideQuote.symbol].updateSnapshot(snapshotTooSideQuote);
                this.ass[snapshotTooSideQuote.symbol].resetHistory(snapshotTooSideQuote);
            };
        };
        // обновление котировкой 
        this.updateQuote = function(shortQuote){
            if(this.ass[shortQuote.symbol]){
                this.ass[shortQuote.symbol].updateShortQuote(shortQuote);
            };
        };
        // подключение графика 
        var _selectedSymbol;
        this.setchartdata = function(smbl){

            var ss = smbl || _selectedSymbol;
            // console.log(ss);
            if(this.ass[ss]){
                var ret = {
                    symbol : this.ass[ss].symbol,
                    labels : this.ass[ss].labels,
                    series : this.ass[ss].series,
                    data : this.ass[ss].data
                };
            } else{
                var ret = {
                    symbol : "no data",
                    labels : [10],
                    series : [10],
                    data : [10]
                };        
            }
            _selectedSymbol = ss;
            // console.log(_selectedSymbol);
            return ret;
        };
    };
    // --- сторидж инструментов (свойства только) с группировкой по group
    function ToolStorage(){
        $gear.UpdatableStorage.apply(this);

        var groupCreator = function(properties){
            return new $gear.Group(properties,'group',properties['group'],'symbol');
        };
        this.groups = new $gear.GroupStorage(groupCreator);

        this.updateToolProperties = function(toolProperies)
        {
            this.addOrUpdate(toolProperies.symbol,toolProperies);
        };
        this.afterAdd = function(name,toolProperies){
            this.groups.add(this.ass[name]);
        };
        this.addOrUpdateBeforeUpdate = function(name,obj){
            this.groups.reLocation(this.ass[obj.symbol].group, obj.group, obj.symbol);
        };
        this.removeTool = function(toolProperties){
            this.groups.removeContent(toolProperties.group, toolProperties.symbol);
            this.remove(toolProperties.symbol)
        };
    };
    //-- экспорт наружу 
    exports.VisualQuoteStorage = VisualQuoteStorage;
    exports.ToolStorage = ToolStorage;
    exports.ToolProperties = ToolProperties;
    exports.ShortQuote = ShortQuote;
    exports.SnapshotTooSideQuote = SnapshotTooSideQuote;
})(this.$dealut ={});