;
function TerminalBusiness(progressbar){
    $gear.BusinessBase.apply(this);
    this.barprogress = progressbar;

    this.startProgress = function(){
        this.barprogress.start();
    };
    this.completeProgress = function(){
        this.barprogress.complete();
    };
    // объект с инструментами и котировками 
    this.tools = new $dealut.ToolStorage();
    // ---
    this.quotes = new $dealut.VisualQuoteStorage(this.cron);
    //---
    this.clearContent = function(){
        // this.startProgress();
        console.log("clearContent");
        this.setchartdata();
        this.tools = new $dealut.ToolStorage();
        // ---
        this.quotes = new $dealut.VisualQuoteStorage(this.cron);
    };
    // вызываемые методы
    this.ToolPropertiesUpdate = function(brick){
        this.tools.updateToolProperties(new $dealut.ToolProperties(brick));
        this.quotes.addOrUpdateToolProperties(new $dealut.ToolProperties(brick));
        this.lazyUI(11);
    };
    
    this.ToolPropertiesRemove = function(brick){
        this.tools.updateToolProperties(new $dealut.ToolProperties(brick));
        this.quotes.removeQuote(new $dealut.ToolProperties(brick));
        this.lazyUI(11);
    };
    this.SnapshotTooSideQuote  = function(brick){
        var tmpsnp = new $dealut.SnapshotTooSideQuote(brick);
        this.quotes.updateSnapshot(tmpsnp);
        resetHistory(tmpsnp);
        this.lazyUI(11);
        // this.completeProgress();
    };
    this.BID = function(brick){
        this.quotes.updateQuote(new $dealut.ShortQuote(brick));
        // this.spotUI();
        this.lazyUI(7);
    };
    this.ASK = function(brick){
        this.quotes.updateQuote(new $dealut.ShortQuote(brick));
        // this.spotUI();
        this.lazyUI(7);
    };
    this.HeartBeatApp = function(brick){
        // console.log(brick.toString());
    };
    this.GreetingApp = function(){};

    this.onConnectionClose = function(){
        this.connectionFlag = false;
        this.spotUI(function(){});
    };
    this.afterLogin = function(){
        // this.startProgress();
        this.connectionFlag = true;
        // this.clearContent();
        // var tmp = this;
        // this.spotUI(function(){
        //     console.log(this);
        //     tmp.setchartdata();
        // });

        
        // this.delayUI(this.setchartdata(selectedSymbol));
    };

    // работаем с графиками проблема с обновлением лэйблов - мерцание:
    // https://github.com/jtblin/angular-chart.js/issues/266
    var _selectedSymbol;
    this.setchartdata = function(arg){
        var tmp = this.quotes.setchartdata(arg);
        this.chart.symbol = tmp.symbol;
        this.chart.data[0] = tmp.data[0];
        _selectedSymbol = arg;
    };
    var tickhistorydept = 60;
    this.chart = 
    {
        symbol : "click symbol in table for select it",
        labels : new Array(tickhistorydept),
        series : ["BID"],
        data : [new Array(tickhistorydept)]

    };
    var _context = this;
    var resetHistory = function(snapshotTooSideQuote)
    {
        if(snapshotTooSideQuote.symbol == _selectedSymbol){
            // console.log(_selectedSymbol);
            _context.setchartdata(_selectedSymbol);
        };
    };

};

function TerminalModel($scope,$timeout, $cookies, ngProgressFactory){
    this.name = "Terminal";
    this.actionHost;
    this.credentials;
    this.business;

    var progressbar = ngProgressFactory.createInstance();
    progressbar.setHeight('5px');
    progressbar.setColor('#40FF00');

    this.appStart = function(){
        if(this.actionHost){
            console.log("старт приложения с существующим экшн-хостом");
            this.actionHost.dispose();
            // старт подключения
            this.actionHost = $gear.getActionHost(this.business,this.credentials,$timeout);
        }
        else {
            this.credentials = new $gear.Credentials($cookies,{ 
                login:'6a19c128-34e8-4c44-adeb-370bc7c2a7c7',
                password:'b8128324',
                role:'TRADER'},
                'terminal'); 
            this.business = new TerminalBusiness(progressbar);
            // старт подключения 
            this.actionHost = $gear.getActionHost(this.business,this.credentials,$timeout);
        };
    };
    
    this.saveCredentials = function(){
        this.credentials.saveInCookie($cookies);
    };
    this.appRestart = function(){
        this.saveCredentials();
        this.actionHost = null;
        // this.actionHost.dispose();
        this.appStart();
    };

    this.appStart();
    this.test = function(arg){
        this.business.setchartdata(arg);
        console.log(this.business.chart);
    };
};
function TerminalMainController($scope,$timeout, $cookies, ngProgressFactory){
    $scope.model = new TerminalModel($scope,$timeout, $cookies, ngProgressFactory);
    var tt = function(){
        $scope.model.business.setchartdata("EURUSD");
    };
    $timeout(tt,1000);
};
var terminalApp = angular.module('terminalApp',['ngRoute','ngCookies','ui.bootstrap','chart.js','ngProgress']);
terminalApp.controller('terminalMainController',['$scope','$timeout','$cookies','ngProgressFactory', TerminalMainController]);
// --- контроллер аккордеона 
terminalApp.controller('GroupsAccordion',['$scope',function ($scope) {
  $scope.oneAtATime = true;
  $scope.instrumentgroups = $scope.model.business.quotes.groups.arr;
  this.test = function(arg){
    alert("accordion")
  };
}]);
terminalApp.controller('ChartAccordion',['$scope',function ($scope) {

}]);
terminalApp.controller('TickChart',['$scope',function ($scope) {

  $scope.labels = ["January", "February", "March", "April", "May", "June", "July"];
  $scope.series = ['Series A', 'Series B'];
  $scope.data = [
    [30, 59, 80, 81, 56, 55, 40],
    [28, 48, 40, 19, 86, 27, 90]
  ];
  $scope.onClick = function (points, evt) {
    console.log(points, evt);
  };
  // $scope.datasetOverride = [{ yAxisID: 'y-axis-1' }, { yAxisID: 'y-axis-2' }];
     $scope.datasetOverride = [];
    $scope.options = {
        // animation : false,
        elements: {
            line: {
                tension: 0
            }
        },
        scales:{
            yAxes:[
                {
                    id: 'y-axis-1',
                    type: 'linear',
                    display: true,
                    position: 'right'
                }
            ]
        }
    // ,
    // scales: {
    //   yAxes: [
    //     {
    //       id: 'y-axis-1',
    //       type: 'linear',
    //       display: true,
    //       position: 'left'
    //     },
    //     {
    //       id: 'y-axis-2',
    //       type: 'linear',
    //       display: true,
    //       position: 'right'
    //     }
    //   ]
    // }
  };

}]);