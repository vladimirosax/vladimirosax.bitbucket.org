;
// --- передаваемые данные
// запрос куска истории 
function SegmentHistoryRequest(brick,left,right){
    this.symbol;
    this.side;
    this.period;
    this.leftOpenTime;
    this.rightOpenTime;

    if(brick != null){
        // в нулевом поле - название метода
        this.symbol = brick.dic[1];
        this.side = brick.dic[2];
        // число минут в интервале
        this.period = brick.inc[0];
        // старейшая граница интервала включительно
        this.leftOpenTime = brick.inc[1];
        // новейшая граница интервала включительно
        this.rightOpenTime = brick.inc[2];
    }
    else{
        this.symbol = 'ALL';
        this.side = 'pop';
        this.period = '333';
        this.leftOpenTime = left;
        this.rightOpenTime = right;
    };

    this.update = function(symbol,side,period,leftOpenTime,rightOpenTime){
        this.symbol = symbol;
        this.side = side;
        this.period = period;
        this.leftOpenTime = leftOpenTime;
        this.rightOpenTime = rightOpenTime;
    };
    this.toBrick = function(){
        return $gear.getBrick(this.toString());
    };
};
// переопределение метода прототипа?
SegmentHistoryRequest.prototype.toString = function(){
    var one = moment(this.leftOpenTime).format('YYYYMMDDHHmm');
    var too = new Date(moment(one));
    //var ttl = headRequest | 'OpenHistoryRequest';
    // console.log(one + '  '+ too);
    return 'HistoryRequestOpen' + 
    '|'+ 
    this.symbol + 
    '|' +
    this.side + 
     '^' + 
    this.period + 
     '|' + 
    moment(this.leftOpenTime).format('YYYYMMDDHHmm') +
     '|' + 
    moment(this.rightOpenTime).format('YYYYMMDDHHmm');
};
// дневной отчет по конкретному клиенту
function DailyStatReport(brick){
    this.name;
    this.date;
    this.usd;
    this.usdAccumulate;
    // восставшие из кирпича 
    if(brick){
        this.name = brick.dic[1];
        // время приходит в UTC, надо что-то с этим делать
        this.date = new Date(moment(brick.inc[0]));
        this.usd = parseFloat(brick.inc[1]);
        this.usdAccumulate = $gear.uParseFloat(brick.inc[2]);
    };
    this.toBrick = function(){
        return $gear.getBrick(this.toString());
    };
    this.toString = function(){
        return 'DailyStatReport' + 
        '|' + 
        this.name + 
        '^' + 
        moment(this.date).format('YYYYMMDDHHmm') +
        '|' + 
        this.usd + 
        '|' + 
        this.usdAccumulate;
    };
};
// пара, валюты, курс, комиссия и название клиента, к которому это относится 
function InstrumentInfo(brick){
    this.name;
    this.symbol;
    this.ccy1;
    this.ccy2;
    this.rate;//это число!
    this.date;//это дата
    this.comiss;//это число!
    // восставшие из кирпича 
    if(brick){
        this.name = brick.dic[1];
        this.symbol = brick.inc[0];
        this.ccy1 = brick.inc[1];
        this.ccy2 = brick.inc[2];
        this.rate = $gear.uParseFloat(brick.inc[3]);
        // время приходит в UTC, надо что-то с этим делать
        // this.date = new Date(moment(brick.inc[4]));
        this.date = moment();
        this.comiss = $gear.uParseFloat(brick.inc[5]);
    };
    this.toBrick = function(){
        return $gear.getBrick(this.toString());
    };
    this.toString = function(){
        return 'InstrumentInfo' + 
        '|' + 
        this.name + 
        '^' + 
        this.symbol +
        '|' + 
        this.ccy1 + 
        '|' + 
        this.ccy2 +
        '|' + 
        this.rate +
        '|' + 
        moment(this.date).format('yyyy.MM.dd-HH:mm:ss') +
        '|' + 
        this.comiss;
    };
    this.update = function(instrumentInfo){
        this.symbol = instrumentInfo.symbol;
        this.ccy1 = instrumentInfo.ccy1;
        this.ccy2 = instrumentInfo.ccy2;
        this.rate = instrumentInfo.rate;
        this.comiss = instrumentInfo.comiss;
        this.date = instrumentInfo.date;
    };
};
// распределение объемов по контрагентам для конкретного клиента (таска)
function DistributionReport(brick){
    this.name;
    this.srcName;
    this.usd;
    this.count;
    if(brick){
        this.srcName = brick.dic[1];
        this.name = brick.inc[0];
        this.usd = $gear.uParseFloat(brick.inc[1]);
        this.count = parseInt(brick.inc[2],10);
    };
    this.toBrick = function(){
        return $gear.getBrick(this.toString());
    };
    this.toString = function(){
        return 'DistributionReport' + 
        '|' + 
        this.srcName + 
        '^' + 
        this.name +
        '|' + 
        this.usd + 
        '|' + 
        this.count;
    };
};
// объем по конкретному инструменту (с именем клиента-таска)
function DistrReportInstr(brick){
    this.name;
    this.instrName;
    this.usd;
    this.volumeCcy1;
    this.volumeCcy2;
    this.count;
    // непередаваемые свойства
    this.convertationPrice;
    this.convertationInstrumentName;

    if(brick){
        this.name = brick.dic[1];
        this.instrName = brick.dic[2];
        this.usd = $gear.uParseFloat(brick.inc[0]);
        this.volumeCcy1 = $gear.uParseFloat(brick.inc[1]);
        this.volumeCcy2 = $gear.uParseFloat(brick.inc[2]);
        this.count = parseInt(brick.inc[3],10);
    };
    this.toBrick = function(){
        return $gear.getBrick(this.toString());
    };
        this.toString = function(){
        return 'DistrReportInstr' + 
        '|' + 
        this.name +
        '|' + 
        this.instrName +
        '^' + 
        this.usd +
        '|' + 
        this.volumeCcy1 + 
        '|' + 
        this.volumeCcy2 + 
        '|' + 
        this.count;
    };
};
//отношение хитов к редждектам по инструменту для таска 
function RejectHitRatio(brick){
    this.name;
    this.lpName;
    this.volume;

    if(brick){
        this.name = brick.dic[1];
        this.lpName = brick.dic[2];
        // this.volume = parseFloat(brick.inc[0].replace(/[,]+/g, '.'));
        this.volume = $gear.uParseFloat(brick.inc[0]);
    };
    this.toBrick = function(){
        // return $gear.getBrick(this.toString());
        return $gear.makeBrick(['RejectHitRatio',this.name,this.lpName],[this.volume]);
    };
        this.toString = function(){
        return 'RejectHitRatio' + 
        '|' + 
        this.name +
        '|' + 
        this.lpName +
        '^' + 
        this.volume;
    };
}
//--- прибыль клиента конкретного таска
function ClientResult(brick){
    this.name;//имя таска
    this.clientName;
    this.volume;

    if(brick){
        this.name = brick.dic[1];
        this.clientName = brick.dic[2];
        this.volume = $gear.uParseFloat(brick.inc[0]);
    };

    this.toBrick = function(){
        return $gear.getBrick(this.toString());
    };
    this.toString = function(){
        return 'ClientResult' + 
        '|' + 
        this.name +
        '|' + 
        this.clientName +
        '^' + 
        this.volume;
    };
}
//---средний размер сделки по инструменту (для таска)
function AverageDealVolume(brick){
    this.name;//имя таска
    this.symbol;
    this.volume;

    if(brick){
        this.name = brick.dic[1];
        this.symbol = brick.dic[2];
        this.volume = $gear.uParseFloat(brick.inc[0]);
    };

    this.toString = function(){
        return 'AverageDealVolume' + 
        '|' + 
        this.name +
        '|' + 
        this.symbol +
        '^' + 
        this.volume;
    };
}
// --- конец передаваемых/принимаемых данных 

// -- классы приложения
// главный бизнес- класс совмещен диспетчером сообщений 
function BusinessReport(progressbar){
    $gear.BusinessBase.apply(this);
    // this.connectionFlag = false;
    this.barprogress = progressbar;

    this.startProgress = function(){
        this.barprogress.start();
    };
    this.completeProgress = function(){
        this.barprogress.complete();
    };
    this.state = new RequestImpossible(this);

    this.clientsCatalog = new ClientsCatalog();

    this.InstrumentInfo = function(brick){
        this.clientsCatalog.updateInstrumentInfo(new InstrumentInfo(brick));
    };
    this.DistributionReport = function(brick){
        this.clientsCatalog.updateDistribution(new DistributionReport(brick));
    };
    this.DistrReportInstr = function(brick){
        this.clientsCatalog.updateInstrumentDistribution(new DistrReportInstr(brick));
    };
    this.DailyStatReport = function(brick){
        this.clientsCatalog.updateDailyStatReport(new DailyStatReport(brick));
    };
    this.RejectHitRatio = function(brick){
        this.clientsCatalog.updateRejectHitRatio(new RejectHitRatio(brick));
    };
    this.ClientResult = function(brick){
        this.clientsCatalog.updateClientResult(new ClientResult(brick));
    };
    this.AverageDealVolume = function(brick){
        this.clientsCatalog.updateAverageDealVolume(new AverageDealVolume(brick));
    };
    this.ClientListCheck = function(brick){
        this.clientsCatalog.updateNames(brick.inc[0].split(';'));
        this.state.endResiveData();
    };
    this.undefinedMethod = function(brick){
        console.log('undefinedMethod - '+ brick.toString());
    };
    this.HeartBeatApp = function(brick){
        // this.stub(brick);
    };
    this.greetingApp = function(brick){
        return this.firstOutcomeMessage;
        // return this.currentRequest.toString();
    };
    // сигнал готовности к общению, или сразу запрос
    this.firstOutcomeMessage = 'GreetingApp^УРА!';

    // запрос интервала данных 
    this.currentRequest;
    this.segmentHistoryRequest = function(segmentHistoryRequest){
        this.currentRequest = segmentHistoryRequest;
        this.state.segmentHistoryRequest(segmentHistoryRequest);
    };

    this.clearContent = function(){
        this.clientsCatalog.clearContent;
    };

    // обязательные методы (переопределения)
    this.onConnectionOpen = function(){
        //this.state = new RequestWasNot(this);
    };
    this.onConnectionClose = function(){
        this.state = new RequestImpossible(this);
    };
    this.afterLogin = function(){
        this.state = new RequestWasNot(this);
        this.segmentHistoryRequest(this.currentRequest);
    };
};
//--- состояния запроса для бизнеса
// запроса небыло
function RequestWasNot(businessReport){
    businessReport.connectionFlag = true;
    businessReport.spotUI(function(){});
    this.segmentHistoryRequest = function(segmentHistoryRequest){
        // меняем состояние бизнеса на "занято"
        businessReport.state = new RequestProcess(businessReport);
        // очистка текущих данных на клиенте 
        businessReport.clearContent();
        // шлем запрос на сервер 
        businessReport.actionHost.sendMessage(segmentHistoryRequest.toBrick());
    };
    this.endResiveData = function(){
        businessReport.state = new RequestCompleted(businessReport);
    };
};
// запрос в процессе обработки
function RequestProcess(businessReport){
    businessReport.startProgress();
    this.segmentHistoryRequest = function(segmentHistoryRequest){
        // вызов игнорируется
    };
    this.endResiveData = function(){
        businessReport.completeProgress();
        // устанавливаем следующее состояние бизнеса ("готов")
        setTimeout(function() 
        { 
            businessReport.state = new RequestCompleted(businessReport);
        }, 333);
    };
};
// запрос завершен, готов продолжать 
function RequestCompleted(businessReport){
    businessReport.spotUI(function(){});
    // обработка очередного запроса 
    this.segmentHistoryRequest = function(segmentHistoryRequest){
        // businessReport.showProgress = true;
        // меняем состояние бизнеса на "занято"
        businessReport.state = new RequestProcess(businessReport);
        // очистка текущих данных на клиенте 
        businessReport.clearContent();
        // шлем запрос на сервер 
        businessReport.actionHost.sendMessage(segmentHistoryRequest.toBrick());
    };
    this.endResiveData = function(){
        // не реагируем ни как
    };
};
function RequestImpossible(businessReport){
    businessReport.connectionFlag = false;
    businessReport.spotUI(function(){});
    // обработка очередного запроса 
    this.segmentHistoryRequest = function(segmentHistoryRequest){
        alert('запрос невозможен');
    };
    this.endResiveData = function(){
        // не реагируем ни как
    };
};
//--- конец состояний запроса для бизнеса 

// каталог клиентов автопополняемый главный (TaskList, стопка плашек с названиями)
function ClientsCatalog(){
    this.itogo = 0;
    this.payment = 0;
    var _arrayClients = [];
      Object.defineProperty(this, "arrayClients", {
        get: function() {
            return _arrayClients;
        },
        set: function(value) {
            _arrayClients = value;
        }
    });
    // отделяем объект-хранилище без паразитных свойств и полей
    var _hashTable = Object.create(null);
    Object.defineProperty(this,"hashTable",{
        get:function(){
            return _hashTable;
        }
    });
    this.updateByName = function(name){
        if(!(name in this.hashTable)){
            var tmp = new DisplayClient(name);
            this.hashTable[tmp.name] = tmp;
            _arrayClients.push(tmp);
            // console.log(tmp.name);
        };
    };
    // обновление по списку имен
    this.updateNames = function(listNames){
        // console.log(listNames);
        for (var i = 0; i < listNames.length; i++) {
            this.updateByName(listNames[i]);
        };
        // console.log(this.arrayClients);
        this.calcSummary();
    };
    // обновление получением конкретного клиента целиком
    this.updateClient = function(displayClient){
        if(displayClient.name in this.hashTable){
            this.hashTable[displayClient.name].update(displayClient);
        }
        else{
            this.hashTable[displayClient.name] = displayClient;
            _arrayClients.push(this.hashTable[displayClient.name]);
        };
    };
    // обновление получением дневного репорта конкретного клиента 
    this.updateDailyStatReport = function(dalyStatReport){
        this.updateByName(dalyStatReport.name);
        this.hashTable[dalyStatReport.name].updateDailyStatReport(dalyStatReport);
    };
    // обновление по свойствам инструмента (вписывает комиссию)
    this.updateInstrumentInfo = function(instrumentInfo){
        this.updateByName(instrumentInfo.name);
        this.hashTable[instrumentInfo.name].updateInstrumentInfo(instrumentInfo);
    };
    // обновление распределением объемов по инструменту
    this.updateInstrumentDistribution = function(instrumentDistribution){
        this.updateByName(instrumentDistribution.name);
        this.hashTable[instrumentDistribution.name].updateInstrumentDistribution(instrumentDistribution);
    };
    // обновление распределением объемов по контрагентам 
    this.updateDistribution = function(distributionReport){
        this.updateByName(distributionReport.name);
        this.hashTable[distributionReport.name].updateDistribution(distributionReport);
    };
    //обновление реджект\хит отчетом
    this.updateRejectHitRatio = function(rejecHitRatio){
        this.updateByName(rejecHitRatio.name);
        this.hashTable[rejecHitRatio.name].updateRejectHitRatio(rejecHitRatio);
    };
    //обновление клиентским результатом
    this.updateClientResult = function(clientResult){
        this.updateByName(clientResult.name);
        this.hashTable[clientResult.name].updateClientResult(clientResult);
    };
    //обновление средним объемом сделки по инструменту
    this.updateAverageDealVolume = function(averageDealVolume){
        this.updateByName(averageDealVolume.name);
        this.hashTable[averageDealVolume.name].updateAverageDealVolume(averageDealVolume);
    };
    // удаление всех объектов из каталога
    Object.defineProperty(this, "clearContent", {
        get: function() {
            while(_arrayClients.length > 0){
            _arrayClients.pop();
            };
            _hashTable = {};

            this.itogo = 0;
            this.payment = 0;
        }
    });
    this.calcSummary = function(){
        _arrayClients.sort(function(a,b){
            return a.payment > b.payment ? -1:1;
        });
        for(var i = 0; i < _arrayClients.length ; i++){
            this.itogo = this.itogo + _arrayClients[i].itogo;
            this.payment = this.payment + _arrayClients[i].payment;
        };
    };
};
// содержит отчеты и итоги по конкретному клиенту за период (DisplayTask)
function DisplayClient(name){
    this.title ='млн. USD';
    this.name = name;
    this.instrumentInfoCatalog = new InstumentInfoCatalog(name);
    var _itogo = 0;
    var _payment = 0;
    var _comiss = 1;
    // дневные отчеты по объемам и их отображение на графике 
    this.dailyReports = [];
    // для графика подписи к курсору (это всплывает под курсором)
    this.series = [this.title];
    // метки на оси времени (оси X)
    this.labels = [];
    // серия данных (может быть много)
    this.ds = [];
    // набор данных (массив массивов, тут из одного значения - серии)
    this.data  = [this.ds];
    // опции вывода даты 
    var options = {
        month: 'long',
        day: 'numeric',
    };
    // распределение по инструментам
    this.instrDistributionCatalog = new InstrumentDistributionCatalog(name);
    // 
    this.distributionReportCatalog = new DistributionReportCatalog(name);
    //
    this.rejectHitRatioCatalog = new RejectHitRatioCatalog(name);
    //
    this.clientResultCatalog = new ClientResultCatalog(name);
    //
    this.averageDealVolumeCatalog = new AverageDealVolumeCatalog(name);
    // перезапись целиком (не реализовано)
    this.update = function(displayClient){

    };
    this.updateDistribution = function(distributionReport){
        this.distributionReportCatalog.update(distributionReport);
    };
    // обновляем распределение объемов по инструментам 
    this.updateInstrumentDistribution = function(distrReportInstr){
        this.instrDistributionCatalog.update(distrReportInstr);
    };
    this.updateDailyStatReport = function(dailyStatReport){
        this.itogo = dailyStatReport.usdAccumulate;
        this.dailyReports.push(dailyStatReport);
        
        this.labels.push(dailyStatReport.date.toLocaleString("ru", options));
        this.ds.push(Math.round((dailyStatReport.usd / 1000000) * 10) / 10);
    };
    this.updateInstrumentInfo = function(instrumentInfo){
        this.comiss = instrumentInfo.comiss;
        this.instrumentInfoCatalog.update(instrumentInfo);
        // console.log(this.instrumentInfoCatalog);
    };
    //обновление реджект\хит отчетом
    this.updateRejectHitRatio = function(rejectHitRatio){
        this.rejectHitRatioCatalog.update(rejectHitRatio);
    };
    //обновление клиентским результатом
    this.updateClientResult = function(clientResult){
        this.clientResultCatalog.update(clientResult);
    };
    //обновление средним объемом сделки по инструменту
    this.updateAverageDealVolume = function(averageDealVolume){
        this.averageDealVolumeCatalog.update(averageDealVolume);
    };
    Object.defineProperty(this, "itogo", {
        get: function() {
            return _itogo;
        },
        set: function(value) {
            _itogo = value;
            this.payment = _itogo * this.comiss;
        }
    });
    Object.defineProperty(this, "payment", {
        get: function() {
            return _payment;
        },
        set: function(value) {
            _payment = value;
        }
    });
    Object.defineProperty(this, "comiss", {
        get: function() {
            return _comiss;
        },
        set: function(value) {
            if(value != _comiss){
                _comiss = value;
                this.payment = _itogo * _comiss;
            }
        }
    });
};
// локальный каталог инструментов с ценами на день расчетов (не показывается)
function InstumentInfoCatalog(name){
    this.name = name;
    this.arrayInstrumentInfo = [];
    this.hashTable = {};
    this.clearContent = function(){
        while(this.arrayInstrumentInfo.length > 0){
            this.arrayInstrumentInfo.pop();
        };
        this.hashTable = {};
    };
    this.update = function(instrumentInfo){
        if(instrumentInfo.symbol in this.hashTable){
            this.hashTable[instrumentInfo.symbol].update(instrumentInfo);
        }
        else{
            this.hashTable[instrumentInfo.symbol] = instrumentInfo;
            this.arrayInstrumentInfo.push(this.hashTable[instrumentInfo.symbol]);
        };
    };
};
// распределение объемов по инструментам в одном клиенте "name"
function InstrumentDistributionCatalog(name){
    this.name = name;
    this.arraydistrReportInstr = [];
    this.hashTable = {};
    // очистка объекта от данных 
    this.clearContent = function(){
        while(this.arraydistrReportInstr > 0){
            this.arraydistrReportInstr.pop();
        };
        this.hashTable = {};
    };
    this.update = function(distrReportInstr){
        if(distrReportInstr.instrName in this.hashTable){
            // апдейт объекта отчета объема по инструменту не предусмотрен (пока?)
        }
        else{
            // тут просто добавляем отчет в хранилище 
            this.hashTable[distrReportInstr.instrName] = distrReportInstr;
            this.arraydistrReportInstr.push(distrReportInstr);

            // как это сортировать?
            this.labels.push(distrReportInstr.instrName);
            this.ds.push(Math.round((distrReportInstr.usd / 1000000) * 10) / 10);
        }
    };
    // графические прибамбасы 
    this.series = ["млн. USD"];
    // метки на оси времени (оси X)
    this.labels = [];
    // серия данных (может быть много)
    this.ds = [];
    // набор данных (массив массивов, тут из одного значения - серии)
    this.data  = [this.ds];
};
//распределение объемов по контрагентам 
function DistributionReportCatalog(name){
    this.name = name;
    this.array = [];
    this.hashTable = {};

    this.clearContent = function(){
        while(this.array > 0){
            this.array.pop();
        };
        this.hashTable = {};
    };
    this.update = function(distributionReport){
        if(distributionReport.srcName in this.hashTable){
            // апдейт объекта отчета объема по инструменту не предусмотрен (пока?)
        }
        else{
            // тут просто добавляем отчет в хранилище 
            this.hashTable[distributionReport.srcName] = distributionReport;
            this.array.push(distributionReport);
            // console.log(distributionReport.srcName);
            // как это сортировать?
            this.labels.push(distributionReport.srcName);
            this.ds.push(Math.round((distributionReport.usd / 1000000) * 10) / 10);
        }
    };
    // графические прибамбасы 
    this.series = ["млн. USD"];
    // метки на оси времени (оси X)
    this.labels = [];
    // серия данных (может быть много)
    this.ds = [];
    // набор данных (массив массивов, тут из одного значения - серии)
    this.data  = [this.ds];
};
//распределение реджект\хит по инструментам
function RejectHitRatioCatalog(name){
    this.name = name;
    this.array = [];
    this.hashTable = {};

    this.clearContent = function(){
        while(this.array > 0){
            this.array.pop();
        };
        this.hashTable = {};
    };

    this.update = function(rejertHitRatio){
        if(rejertHitRatio.lpName in this.hashTable){
            // существующее значение не обновляется 
        }
        else{
            // тут просто добавляем отчет в хранилище 
            this.hashTable[rejertHitRatio.lpName] = rejertHitRatio;
            this.array.push(rejertHitRatio);
            // console.log(rejertHitRatio.lpName);
            // как это сортировать?
            this.labels.push(rejertHitRatio.lpName);
            this.ds.push(rejertHitRatio.volume);
            //this.ds.push(Math.round((distributionReport.usd / 1000000) * 10) / 10);
        }
    };

    // графические прибамбасы 
    this.series = ["Reject %"];
    // метки на горизонтальной оси (оси X)
    this.labels = [];
    // серия данных (может быть много)
    this.ds = [];
    // набор данных (массив массивов, тут из одного значения - серии)
    this.data  = [this.ds];
};
//каталог результатов клиентов
function ClientResultCatalog(name){
    this.name = name;
    this.array = [];
    this.hashTable = {};

    this.clearContent = function(){
        while(this.array > 0){
            this.array.pop();
        };
        this.hashTable = {};
    };

    this.update = function(clienResult){
        if(clienResult.clientName in this.hashTable){
            // существующее значение не обновляется 
        }
        else{
            // тут просто добавляем отчет в хранилище 
            this.hashTable[clienResult.clientName] = clienResult;
            this.array.push(clienResult);
            //console.log(clienResult.clientName);
            this.labels.push(clienResult.clientName);
            this.ds.push(clienResult.volume);
            this.series.push(clienResult.clientName);
            //this.ds.push(Math.round((distributionReport.usd / 1000000) * 10) / 10);
        }
    };

    // графические прибамбасы 
    this.series = [];
    // метки на горизонтальной оси (оси X)
    this.labels = [];
    // серия данных (может быть много)
    this.ds = [];
    // набор данных (массив массивов, тут из одного значения - серии)
    this.data  = [this.ds];
};
//каталог средних объемов по инструментам
function AverageDealVolumeCatalog(name){
    this.name = name;
    this.array = [];
    this.hashTable = {};

    this.clearContent = function(){
        while(this.array > 0){
            this.array.pop();
        };
        this.hashTable = {};
    };

    this.update = function(averageDealVolume){
        if(averageDealVolume.symbol in this.hashTable){
            // существующее значение не обновляется 
        }
        else{
            // тут просто добавляем отчет в хранилище 
            this.hashTable[averageDealVolume.symbol] = averageDealVolume;
            this.array.push(averageDealVolume);
            //console.log(clienResult.clientName);
            this.labels.push(averageDealVolume.symbol);
            this.ds.push(averageDealVolume.volume);
            //this.ds.push(Math.round((distributionReport.usd / 1000000) * 10) / 10);
        }
    };

    // графические прибамбасы 
    this.series = ["USD "];
    // метки на горизонтальной оси (оси X)
    this.labels = [];
    // серия данных (может быть много)
    this.ds = [];
    // набор данных (массив массивов, тут из одного значения - серии)
    this.data  = [this.ds];
};
//--- конец классов приложения

function ReportModel($scope,$timeout, $cookies, ngProgressFactory){
    this.name = "ReportModel";
    this.actionHost;
    this.credentials;
    this.business;
    var progressbar = ngProgressFactory.createInstance();
    progressbar.setHeight('5px');
    progressbar.setColor('#40FF00');
    
    // ---
    this.saveCredentials = function(){
        // alert('Try save credentials');
        this.credentials.saveInCookie($cookies);
    };
    this.setInterval = function(leftEdge,rightEdge){
        var sehr = new SegmentHistoryRequest(null,leftEdge, rightEdge);
        this.business.segmentHistoryRequest(sehr);
    };
    // метод старта (рестарта) не меняет бизнес. вызывать после изменения креденциалов
    this.appStart = function(){
        if(this.actionHost){
            this.actionHost.dispose();
            // старт подключения
            this.actionHost = $gear.getActionHost(this.business,this.credentials,$timeout);
        }
        else {
            this.credentials = new $gear.Credentials($cookies); 
            this.business = new BusinessReport(progressbar);
            // отладочный эпизод - пустая заглушка
            this.business.clientsCatalog.updateByName("");
            // старт подключения 
            this.actionHost = $gear.getActionHost(this.business,this.credentials,$timeout);
        };
    };
    // тут стартует подключение к серверу
    this.appStart();
    // сохранение параметров и повторный запуск
    this.appRestart = function(){
        this.saveCredentials();
        this.appStart();
    };
    // стили для рисования объемов и денег
    this.styles = {
        paymentStyle : {
            "border-radius" : "3px",
            "color" : "white",
            "background-color" : "green",
            "font-size" : "14px",
            "padding" : "3px"
        },
        itogoStyle : {
            "border-radius" : "3px",
            "color" : "white",
            "background-color" : "black",
            "font-size" : "14px",
            "padding" : "3px"
        }
    }
};
function ReportMainController($scope,$timeout, $cookies, ngProgressFactory){
    var reportModel = new ReportModel($scope, $timeout, $cookies, ngProgressFactory);
    $scope.visi = false;
    $scope.model = reportModel;
    $scope.name = "Main controller of my application";
};
function ChartControllerTest($scope){
};
// тело приложения - ангуляр 
var reportApp = angular.module('reportApp',['ngRoute','ngCookies','ui.bootstrap','chart.js','ngProgress']);
reportApp.controller('reportMainController',['$scope','$timeout','$cookies','ngProgressFactory', ReportMainController])
.controller('DatepickerPopupDemoCtrl',['$scope',function ($scope) {
    // дефолтная установка дат. закомметированное делает дату числом миллисекунд
    // $scope.leftDate = new Date().setDate(1);
    // $scope.leftDate.setDate(1);
    $scope.leftDate = new Date(new Date().getFullYear(),new Date().getMonth(),1);
    // $scope.rightDate = new Date();
    $scope.rightDate = new Date(new Date().getFullYear(),new Date().getMonth(),new Date().getDate());
    $scope.model.business.currentRequest = new SegmentHistoryRequest(null, $scope.leftDate, $scope.rightDate);

    $scope.dt = new Date();
    $scope.setInterval = function(){
        $scope.model.setInterval($scope.leftDate, $scope.rightDate);
        // alert($scope.leftDate +'  '+ $scope.rightDate);
    };
    $scope.today = function() {
    
    $scope.model.name = $scope.dt;

  };
  $scope.today();

  $scope.clear = function() {
    $scope.dt = null;
    $scope.model.name="OOPS!";
  };

  $scope.inlineOptions = {
    customClass: getDayClass,
    minDate: new Date(),
    showWeeks: true
  };

  $scope.dateOptions = {
    dateDisabled: disabled,
    formatYear: 'yy',
    maxDate: new Date(2020, 5, 22),
    minDate: new Date(),
    startingDay: 1
  };

  // Disable weekend selection
  function disabled(data) {
    var date = data.date,
      mode = data.mode;
    return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
  }

  $scope.toggleMin = function() {
    $scope.inlineOptions.minDate = $scope.inlineOptions.minDate ? null : new Date();
    $scope.dateOptions.minDate = $scope.inlineOptions.minDate;
  };

  $scope.toggleMin();

  $scope.open1 = function() {
    $scope.popup1.opened = true;
  };

  $scope.open2 = function() {
    $scope.popup2.opened = true;
  };

  $scope.setDate = function(year, month, day) {
    $scope.dt = new Date(year, month, day);
  };

  $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
  $scope.format = $scope.formats[0];
  $scope.altInputFormats = ['M!/d!/yyyy'];

  $scope.popup1 = {
    opened: false
  };

  $scope.popup2 = {
    opened: false
  };

  var tomorrow = new Date();
  tomorrow.setDate(tomorrow.getDate() + 1);
  var afterTomorrow = new Date();
  afterTomorrow.setDate(tomorrow.getDate() + 1);
  $scope.events = [
    {
      date: tomorrow,
      status: 'full'
    },
    {
      date: afterTomorrow,
      status: 'partially'
    }
  ];

  function getDayClass(data) {
    var date = data.date,
      mode = data.mode;
    if (mode === 'day') {
      var dayToCheck = new Date(date).setHours(0,0,0,0);

      for (var i = 0; i < $scope.events.length; i++) {
        var currentDay = new Date($scope.events[i].date).setHours(0,0,0,0);

        if (dayToCheck === currentDay) {
          return $scope.events[i].status;
        }
      }
    }

    return '';
  }
}])
.controller('AccordionDemoCtrl', ['$scope',function ($scope) {
  $scope.oneAtATime = true;
  $scope.groups = $scope.model.business.clientsCatalog.arrayClients;
}])
.controller("BarCtrl", ['$scope',ChartControllerTest])
.controller('TabsDemoCtrl', function ($scope, $window) {
})
.filter('formatDate', function(){
    var options = {
        month: 'long',
        day: 'numeric',
    };
    return function(date){
        return date.toLocaleString("ru", options);
    };
})
.filter('formatVolume', function(){
    return function(volume){
        return Math.round((volume / 1000000) * 10) / 10 ;
    };
})
.filter('formatPayment', function(){
    return function(volume){
        return Math.round((volume) * 100) / 100 ;
    };
});