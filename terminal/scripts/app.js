;
function TerminalBusiness(){
    $gear.BusinessBase.apply(this);
    // объект с инструментами и котировками 
    this.tools = new $dealut.ToolStorage();
    // ---
    this.quotes = new $dealut.VisualQuoteStorage(this.cron);
    // вызываемые методы
    this.ToolPropertiesUpdate = function(brick){
        this.tools.updateToolProperties(new $dealut.ToolProperties(brick));
        this.quotes.addOrUpdateToolProperties(new $dealut.ToolProperties(brick));
        this.lazyUI(150);
    };
    
    this.ToolPropertiesRemove = function(brick){
        this.tools.updateToolProperties(new $dealut.ToolProperties(brick));
        this.quotes.removeQuote(new $dealut.ToolProperties(brick));
        this.lazyUI(150);
    };
    this.SnapshotTooSideQuote  = function(brick){
        this.quotes.updateSnapshot(new $dealut.SnapshotTooSideQuote(brick));
        // console.log(brick);
        this.lazyUI(150);
    };
    this.BID = function(brick){
        this.quotes.updateQuote(new $dealut.ShortQuote(brick));
        this.spotUI();
    };
    this.ASK = function(brick){
        this.quotes.updateQuote(new $dealut.ShortQuote(brick));
        this.spotUI();
    };
    this.HeartBeatApp = function(brick){
        // console.log(brick.toString());
    };
};
function TerminalModel($scope,$timeout, $cookies){
    // window.addEventListener('focus', function() {
    // window.scroll(0,0);
    // });
    
    this.name = "Terminal";
    this.actionHost;
    this.credentials;
    this.business;
    this.appStart = function(){
        if(this.actionHost){
            this.actionHost.dispose();
            // старт подключения
            this.actionHost = $gear.getActionHost(this.business,this.credentials,$timeout);
        }
        else {
            this.credentials = new $gear.Credentials($cookies,{ 
                login:'6a19c128-34e8-4c44-adeb-370bc7c2a7c7',
                password:'b8128324',
                role:'TRADER'},
                'terminal'); 
            this.business = new TerminalBusiness();
            // старт подключения 
            this.actionHost = $gear.getActionHost(this.business,this.credentials,$timeout);
        };
    };
    this.appStart();
    this.saveCredentials = function(){
        this.credentials.saveInCookie($cookies);
    };
    this.appRestart = function(){
        this.saveCredentials();
        this.appStart();
    };
};
function TerminalMainController($scope,$timeout, $cookies){
    $scope.model = new TerminalModel($scope,$timeout, $cookies);
};
var terminalApp = angular.module('terminalApp',['ngRoute','ngCookies','ui.bootstrap','chart.js']);
terminalApp.controller('terminalMainController',['$scope','$timeout','$cookies', TerminalMainController]);