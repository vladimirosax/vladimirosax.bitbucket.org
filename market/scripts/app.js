;
// главный бизнес-класс
function Business(){
    // console.log('конструктор - business');
    var market;

    Object.defineProperty(this, "market", 
    {
        enumerable: false,
        get:function(){
            return market = market ? market : new QuoteOverview('Обзор рынка');
        }
    });

};
// свойства инструментов (тут имеет вспомогательную роль)
function ToolProperties(brick){
    // var brick = brick;
    this.symbol = brick.inc[11];
    this.digits = brick.inc[1];
    this.CCY1 = brick.inc[5];
    this.CCY2 = brick.inc[6];
}
// интерпретатор команд. кирпичи поступают расжатыми
function CommandInterpreter(business){
    // var business = business;
    this.call = function(brick){
        switch (brick.dic[0]) {
        case 'ToolPropertiesUpdate':
            var tmp = new ToolProperties(brick);
            // console.log(tmp.symbol + ' - ' + tmp.digits);
            business.market.push(new Quote(tmp.symbol,'-','-', tmp.digits , tmp.CCY1 , tmp.CCY2));
            break;
        case 'ToolPropertiesRemove':
            var tmp = new ToolProperties(brick);
            business.market.remove(new Quote(tmp.symbol,'-','-', tmp.digits , tmp.CCY1 , tmp.CCY2));
            break;
        case 'HeartBeatApp':
            // console.log('-ping-');
            break;
        case 'BID':
            var tmpsqb = new ShortQuote(brick);
            business.market.updateShort(tmpsqb);
            // console.log('BID');
            break;
        case 'ASK':
            var tmpsqa = new ShortQuote(brick);
            business.market.updateShort(tmpsqa);
            // console.log('ASK');
            break;
        default:
            console.log('Я таких значений не знаю');
        }
    }

};
// класс связи с сервером 
function ConnectSocketServer(credentials,onOpen,onMessage,onClose,checkPass,onLogin){

    var innerState = false;
    var support = "MozWebSocket" in window ? 'MozWebSocket' : ("WebSocket" in window ? 'WebSocket' : null);
    if(!support)
    { 
        alert('WebSocket is not supported');
        return;
    }
    var ws;
    console.log('conecting to:  '+ credentials.endPoint);
    start(credentials.wsLocation);

    
    function start(wslocation){
        ws = new window[support](credentials.wsLocation);
        ws.onclose = function(){
            onClose();
            setTimeout(function(){
                start(credentials.wsLocation)
            }, 5000);
        }
        ws.onopen = function(){
            onOpen();
            ws.send(credentials.handShake);
        };
        ws.onmessage = function(evt){
            if(checkPass(evt.data)){
                var tmp = onLogin();
                if(tmp){
                    innerSendMessage(tmp);
                    ws.onmessage = onMessage;
                }

            }
            else{
                // неудачный ответ от сервера 
            }
            
        };
    };
    
    function innerSendMessage(msg){
        if (ws) {
            // console.log('- sendTest'); 
            ws.send(msg);
        }
    };

    // отправка данных через сокет 
    this.send = function (msg) { 
        innerSendMessage(msg);
    };
};
// креденциалы клиента 
function Credentials(){
        this.role ='TRADER';
        this.login ='6a19c128-34e8-4c44-adeb-370bc7c2a7c7';
        this.responce = 'Success';
        this.nowTimeMs = '636021124868975441';
        this.serverToken = '';
        this.password = 'b8128324';
        this.cryptMode = 'SSLMODE';
        this.pressMode = '1';
        // не передается на сервер 
        // var _endPoint = '172.16.1.109:2012';
        // var _endPoint = '172.16.1.109:5024';
        // var _endPoint = '127.0.0.1:5024';
        var _endPoint = '193.124.59.238:5024';
        // var _endPoint = '127.0.0.1:5024';
        // var _endPoint = '127.0.0.1:5012';

        Object.defineProperty(this, "handShake", 
        {
            enumerable: false,
            get: function (){
                var result = '';
                var delimiter = '';

                for (key in this) {
                    result += this[key] +';';
                    delimiter = ';';
                }
            return result.slice(0,-1);
        }
        });
        Object.defineProperty(this,"endPoint",
        {
            enumerable: false,
            get:function(){
                return _endPoint;
            },
            set:function(value){
                _endPoint = value;
            }
        });
        Object.defineProperty(this,"wsLocation",
        {
            enumerable: false,
            get:function(){
                return 'ws://' + this.endPoint;
            },
        });

        Object.defineProperty(this, "update", 
        {
            enumerable: false,
            set:function(value){
                this.role = value;
            }
        });
        Object.defineProperty(this, "endPoint", {enumerable: false});
};
// полуквота 
function ShortQuote(brick){
    var brick = brick;
    this.symbol = brick.dic[1];
    this.side = brick.dic[0];
    this.price = brick.inc[0];

}
ShortQuote.prototype.symbol;
ShortQuote.prototype.side;
ShortQuote.prototype.price;
// таблица двусторонних котировок 
function QuoteOverview(name){
    // console.log('конструктор - QuoteOverview')
    var sortSign = -1;
    this.resort = function(sortField){
        var sortField = sortField || 'symbol';
        this.quotesArray.sort(function(a,b){
            return sortSign * (a[sortField] > b[sortField] ? -1:1);
        });
        sortSign *=-1;
    };
    this.name = name || 'Обзор рынка';
    // делаем свойство невидимым для перечислителей 
    Object.defineProperty(this, "name", {enumerable: false});
    // определяем стили на уровне таблицы для передачи в ячейки
    this.styles = {
        BID:{
            "border-radius" : "3px",
            "color" : "black",
            "background-color" : "white",
            "font-size" : "14px",
            "padding" : "3px"
        },
        ASK:{
            "border-radius" : "3px",
            "color" : "black",
            "background-color" : "white",
            "font-size" : "14px",
            "padding" : "3px"
        },
        upStyle:{
            "border-radius" : "3px",
            "color" : "black",
            "background-color" : "palegreen",
            "font-size" : "14px",
            "padding" : "3px"
        },
        downStyle:{
            "border-radius" : "3px",
            "color" : "white",
            "background-color" : "red",
            "font-size" : "14px",
            "padding" : "3px"
        },
        neutral:{
            "border-radius" : "3px",
            "color" : "black",
            "background-color" : "white",
            "font-size" : "14px",
            "padding" : "3px"
        },
        allowStyles:true
    };
    Object.defineProperty(this, "styles", {enumerable: false});
    // добавка символа в объект (без котировок) и с проверкой допустимости символа
    Object.defineProperty(this,"updateSymbol", {enumerable: false},{
        get:function(){
            // не реализовано 
        },
        set:function(){
            // не реализовано 
        }
    })


    this.quotesArray = [];
}
QuoteOverview.prototype.name;
// обновление цены в существующей квоте короткой квотой
QuoteOverview.prototype.updateShort = function(shortQuote){
    if(shortQuote.symbol in this){
        this[shortQuote.symbol].update(shortQuote);
    }
};
// добавление новой квоты или обновление свойств инструмента существующей (без цены)
QuoteOverview.prototype.push = function(quote){
    // console.log(quote);
    var tobj = this[quote.symbol];
    if(!(tobj)) {
        this[quote.symbol] = quote;
        // передаем ссылку на стили 
        this[quote.symbol].parentStyles = this.styles;
        // отправляем ссылку на котировку в массив, индексированный числами
        this.quotesArray.push(quote);
    }
    else{
        // var tmp = this[quote.symbol];
        tobj.digits = quote.digits;
        tobj.CCY1 = quote.CCY1;
        tobj.CCY2 = quote.CCY2;
    }
};
QuoteOverview.prototype.remove = function(quote){
    delete this[quote.symbol];
};
// котировка двусторонняя
function Quote(symbol,bid,ask,digits,ccy1,ccy2){
    this.symbol = symbol || '--';
    this.BID = bid ||'--';
    this.ASK = ask || '---';
    // свойства инструмента лежат именно тут(в отличии от десктопа)
    this.CCY1 =ccy1;
    this.CCY2 = ccy2;
    this.digits = digits || 5;
    this.time = '--';
    this.fullTime ='--';
    this.prevPrice;
    // визуальные стили
    this.allowStyles = true;
    // локальные стили
    this.styles = {
        BID:'',
        ASK:''
    };
};
Quote.prototype.parentStyles;
Quote.prototype.allowStyles;
Quote.prototype.symbol;
Quote.prototype.BID;
Quote.prototype.ASK;

Quote.prototype.update = function(shortQuote){
    // получаем обновляемое значение (старое)
    // var updated = this[shortQuote.side];
    // передаем новое и старое значение получаем стиль для стороны котировки[BID\ASK]
    this.styles[shortQuote.side] = this.allowStyles ? 
    this.setVisualStyle(shortQuote.price,this[shortQuote.side]) : this.parentStyles.neutral;

    // теперь обновляем старое значение новым
    this[shortQuote.side] = shortQuote.price;

    // дата и время (дико жрет память и тормозит)
    // this.fullTime =  Date.now()-3600 * 3 * 1000;
    // this.time = new Date(this.fullTime).toLocaleString("ru",{timezone: 'UTC',hour: 'numeric',minute: 'numeric',second: 'numeric'});

    // this.time = this.fullTime.toLocaleString("ru",{timezone: 'UTC',hour: 'numeric',minute: 'numeric',second: 'numeric'});
};
// установка визуального стиля
Quote.prototype.setVisualStyle = function(priceNow,priceOld){
        if(priceOld > priceNow) return this.parentStyles.downStyle;
        if(priceOld < priceNow) return this.parentStyles.upStyle;
        if(priceOld == priceNow) return this.parentStyles.neutral;
};
// чтение куки
function getCookie(name) {
  var matches = document.cookie.match(new RegExp(
    "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
  ));
  return matches ? decodeURIComponent(matches[1]) : undefined;
}
// запись куки 
function setCookie(name, value, options) {
  options = options || {};

  var expires = options.expires;

  if (typeof expires == "number" && expires) {
    var d = new Date();
    d.setTime(d.getTime() + expires * 1000);
    expires = options.expires = d;
  }
  if (expires && expires.toUTCString) {
    options.expires = expires.toUTCString();
  }

  value = encodeURIComponent(value);

  var updatedCookie = name + "=" + value;

  for (var propName in options) {
    updatedCookie += "; " + propName;
    var propValue = options[propName];
    if (propValue !== true) {
      updatedCookie += "=" + propValue;
    }
  };

  document.cookie = updatedCookie;
}
// функция роутера приложения 
function Router($routeProvider){
    $routeProvider
    .when('/main',{
        templateUrl:'pages/first.html',
        controller:'firstController'
    })
    .when('/second',{
        templateUrl:'pages/second.html',
        controller:'secondController'
    });
};
// функция главного контроллера приложения 
function MainController($scope,$timeout){
    $scope.name='mainController';
    $scope.checked = true;
    
    $scope.qover = new QuoteOverview('test of header');

    var nqb = new ShortQuote('AUDUSD','BID',1.5557);
    var nqa = new ShortQuote('AUDUSD','ASK',1.5559);
    $timeout(function(){ 
        $scope.qover.update(nqb);
        $scope.qover.update(nqa);
        $scope.qover.push(new Quote('AUDCHF',4.5623,4.5625) );
        $scope.qover.push(new Quote('GBPCHF',4.5623,4.5625) );
        $scope.qover.push(new Quote('AUDNZD',4.5623,4.5625) );
        $scope.qover.push(new Quote('NZDCHF',4.5623,4.5625) );
    },4000);
};
// тело приложения
// var showApp = angular.module('showApp',['ngRoute','chart.js']);
var showApp = angular.module('showApp',['ngRoute']);
// маршрутизатор приложения 
showApp.config(Router);
// главный контроллер 
showApp.controller('mainController',function($scope, $timeout){
    // $scope.alert = function(){alert('');};
    $scope.sortparam = 'symbol';
    $scope.name='mainController';
    $scope.checked = true;
    $scope.compressed = '';
    $scope.decompressed = '';
    $scope.income = ';'

    if (!navigator.cookieEnabled) {
        alert( 'Включите cookie для комфортной работы с этим сайтом' );
    };
    var business = new Business();
    
    var rpc = new CommandInterpreter(business);
    $scope.market = business.market;
    $scope.instrsCount = Object.keys(business.market).length;
    
    $scope.credentials = new Credentials();
    // счетчик вызовов
    $scope.callCounter = 0;
    var lastCount = 0;
    $scope.callDensity = 0;
    var intervalMs = 10000;
    var timerId = setInterval(function(){
        var current = $scope.callCounter;
        $scope.callDensity = ~~((current - lastCount)/intervalMs*1000);
        lastCount = current;
    },intervalMs);
    // единый конвертер 
    $scope.converter;
    $scope.forceConverter;
    // прямо сразу создаем соединение 
    var conServ = new ConnectSocketServer($scope.credentials,
    // onOpen
    function()
    {
        console.log('connect OK');
    },
    // onMessage
    function(evt){
        var brickRaw = $gear.getBrick(evt.data);
        $timeout(function(){
            var brick = $scope.converter.unpress(brickRaw);
            $scope.compressed = brickRaw.toString();
            $scope.decompressed = brick.toString();
            rpc.call(brick);
            $scope.callCounter += 1;
        });
    },
    // onClose
    function(){
        // console.log(' - closed');
    },
    // checkPass
    function(credentials){
        return true;
    },
    // signal 
    function(){
        // тип конвертера надо сделать зависимым от креденциалов
        // $scope.converter = new Converter($scope.credentials.pressMode);
        $scope.converter = $gear.getGear($scope.credentials.pressMode);
        // $scope.forceConverter = new Converter('1');//подлючка без сжатия
        return $scope.converter.press($gear.getBrick('GreetingApp^УРА!'));
    });
    // посыл сигнала готовности серверу вебсокетов вручную
    // $scope.tryConnect = function(){
    //     // просто разрешаем обмен 
    //     // conServ.send('GreetingApp^УРА!');
    //     if($scope.converter){
    //         conServ.send($scope.converter.press(new Brick('GreetingApp^УРА!')));
    //     }
        
    // };


    $scope.testAct = function(){
        var tt = new Brick('ToolPropertiesUpdate|Default^unknown|5|0|0|0|NZD|CHF|0|False|False|False|NZDCHF');
        rpc.call(tt);
        // $scope.qover = business.market;
        
    };
    $scope.reset = function(){
        // $scope.refresh();
    };
    $scope.myObj = {
        "border-radius" : "5px",
        "color" : "white",
        "background-color" : "coral",
        "font-size" : "14px",
        "padding" : "3px"
    }
    $scope.style0 = {
        // "width": "200px",
        "border-radius" : "5px",
        "color" : "white",
        "background-color" : "coral",
        "font-size" : "14px",
        "padding" : "3px"
    };

});

showApp.controller('firstController',function($scope){
    $scope.name='firstController';
});

showApp.controller('secondController',function($scope){
    $scope.name='secondController';
});
// charting-test
showApp.controller('LineCtrl', ['$scope', '$timeout', function ($scope, $timeout) {
    $scope.labels = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];
    $scope.series = ['Series A', 'Series B'];
    $scope.data = [
      [65, 59, 80, 81, 56, 55, 40],
      [28, 48, 40, 19, 86, 27, 90]
    ];
    $scope.onClick = function (points, evt) {
      console.log(points, evt);
    };
    $scope.onHover = function (points) {
      if (points.length > 0) {
        console.log('Point', points[0].value);
      } else {
        console.log('No point');
      }
    };

    $timeout(function () {
      $scope.labels = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
      $scope.data = [
        [28, 48, 40, 19, 86, 27, 90],
        [65, 59, 80, 81, 56, 55, 40]
      ];
      $scope.series = ['Series C', 'Series D'];
    }, 3000);
  }]);
//   bar-controller
showApp.controller("BarCtrl", function ($scope) {
  $scope.labels = ['2006', '2007', '2008', '2009', '2010', '2011', '2012'];
  $scope.series = ['Series A', 'Series B','Legend - C'];

  $scope.data = [
    [65, 59, 80, 81, 56, 55, 40],
    [28, 48, 40, 19, 86, 27, 90],
    [55, 38, 20, 19, 76, 47, 70]
  ];
});
// фильтр по валютным парам
showApp.filter('symbolSet',function(){
    return function(text){
        return text;
    }
});